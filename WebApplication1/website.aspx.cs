﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{

    public class Zodis
    {
        public string zodis { get; set; }
        public List<Raide> Raides { get; set; } //Kiekviena zodzio raide yra Klases Raide objektas
        public int kiekis { get; private set; }//Skaitliukas nurodantis kuris raidziu rinkinio elementas buvo grazintas 
                                               //paskutini karta

        public Zodis(string line)
        {
            zodis = line;
            Raides = new List<Raide>();
            kiekis = -1;
            for (int i = 0; i < zodis.Length; i++)
            {
                Raides.Add(new Raide(zodis[i]));
            }
        }

        public void PriskirtiRaidesKoordinates(int eilute, int stulpelis)
        {
            Raides[kiekis].eilute = eilute;
            Raides[kiekis].stulpelis = stulpelis;

        }

         //Ieskvieciant si metoda, kiekviena karta grazinama sekanti zodzio raide
        public char Gauti()
        {
            kiekis++;
            return Raides[kiekis].raide;
        }

        //metodas leidziantis atlikti "zingsni atgal" ir dirbti su raide, einancia pries paskutine grazinta zodzio raide
       public void ZingsnisAtgal()
        {
            kiekis--;
        }
        
        public void KoordinaciuIssaugojimas(int eilute, int stulpelis)
        {
            Raides[kiekis].TinkamuRaidzioKoord.Add(eilute);
            Raides[kiekis].TinkamuRaidzioKoord.Add(stulpelis);
        }
        /// <summary>
        /// Metodas grazinantis vis naujas koordinates, kuriose yra ta pati raide. Jeigu koordinaciu nebera, 
        /// vietoje paskutinio irasyto skaiciaus irasoma pries tai toje pozicijoje buvusi raide. Tada atliekamas 
        /// "zingsnis atgal" ir pradedama nagrineti raide, kuri zodyje eina pries paskutine nagrineta raide ir metodas kartoja
        /// ta pati veiksma tol, kol yra randama raide, turinti nepanaudotu koordinaciu, arba kol nera pasalinami visi irasyti
        /// skaiciai. Antru atveju pirma zodzio raide pradeda ieskoti is naujo.
        /// </summary>
        /// <param name="Kryziazodis"></param>
        /// <param name="zodis"></param>
        /// <param name="skaicius"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public Tuple<int, int> GautiKoordinates(ref List<List<char>> Kryziazodis, Zodis zodis, int skaicius, Table table)
        {
            if (Raides[kiekis].zingsnis < Raides[kiekis].TinkamuRaidzioKoord.Count)
            {
                Raides[kiekis].zingsnis += 2;
                return new Tuple<int, int>(Raides[kiekis].TinkamuRaidzioKoord[Raides[kiekis].zingsnis - 2], Raides[kiekis].TinkamuRaidzioKoord[Raides[kiekis].zingsnis - 1]);
            }
            else
            {
                if (kiekis < zodis.zodis.Length)
                {
                    if (kiekis > 0)
                    {

                        Raides[kiekis].TinkamuRaidzioKoord.Clear();
                        Raides[kiekis].zingsnis = 0;
                        ZingsnisAtgal();
                        Kryziazodis[Raides[kiekis].eilute][Raides[kiekis].stulpelis] = Raides[kiekis].raide;
                        Tuple<int, int> NaujosKoordinates = GautiKoordinates(ref Kryziazodis, zodis, skaicius, table);
                        if (NaujosKoordinates != null)
                        {
                            PriskirtiRaidesKoordinates(NaujosKoordinates.Item1, NaujosKoordinates.Item2);
                            string line = skaicius.ToString();
                            char irasomasSkaicius = Convert.ToChar(line);
                            if(ZodziuPaieska.ryskint)
                                Kryziazodis[NaujosKoordinates.Item1][NaujosKoordinates.Item2]= Char.ToUpper(Kryziazodis[NaujosKoordinates.Item1][NaujosKoordinates.Item2]);
                            else
                            Kryziazodis[NaujosKoordinates.Item1][NaujosKoordinates.Item2] = irasomasSkaicius;
                            website.Pildymas(table, NaujosKoordinates.Item1, NaujosKoordinates.Item2, ref Kryziazodis, zodis, false, skaicius);
                        }
                        }

                    else
                        Kryziazodis[Raides[kiekis].eilute][Raides[kiekis].stulpelis] = Raides[kiekis].raide;
                    website.Paieska(table, zodis, ref Kryziazodis, skaicius, Raides[kiekis].eilute, Raides[kiekis].stulpelis, Raides[kiekis].raide);


                }
            }
            return null;
        }
    }

  //Kiekviena ieskomo zodzio raide yra atskiras sios klases objektas. 
    public class Raide
    {
   
        public char raide { get; set; }
        public List<int> TinkamuRaidzioKoord { get; set; } //Ieskant raides aplink jau irasyta pozicija, 
                                                         //visos koordinates kuriose ji buvo surasta irasomos i si parametra
        public int zingsnis { get; set; }    //Parametras nurodantis kelinta karta yra grazinamos raides koordinates
        public int eilute { get; set; }      //Parametras rodo kurioje eiluteje si raide pakeista reikiamu skaiciumi
        public int stulpelis { get; set; }   //Parametras rodo kuriame stulpelyje si raide  pakeista reikiamu skaiciumi

        public Raide(char simbolis)
        {
            raide = simbolis;
            TinkamuRaidzioKoord = new List<int>();

        }
        
        //Turimu koordinaciu grazinimas, kas karta iskvietus metoda grazinamos naujos koordinates
        public Tuple<int, int> Gauti()
        {
            zingsnis++;
            return new Tuple<int, int>(TinkamuRaidzioKoord[zingsnis - 1], TinkamuRaidzioKoord[zingsnis]);
        }

    }
    
    class ZodziuPaieska
    {
        public static int kiekis { get; set; }//Skaitliukas. Kelintas zodis yra ieskomas
        public static List<string> Zodziai { get; set; } //Ieskomu zodziu sarasas
        public static bool ryskint { get; set; }
    }


    public partial class website : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
                string[] visosEilutes =
       File.ReadAllLines(Server.MapPath("App_Data/TextFile1.txt"));
            ZodziuPaieska.ryskint = false;
                List<List<char>> Kryziazodis = new List<List<char>>();
                List<string> Zodziai = new List<string>();
            List<List<char>> KryziazodisNew = new List<List<char>>();
            DuomenuApdorojimas(ref Kryziazodis, ref Zodziai, visosEilutes);
                ZodziuPaieska.kiekis = -1;
                ZodziuPaieska.Zodziai = Zodziai;
            PradiniaiDuomenys(Table4, Kryziazodis);
            string input = null;

            Veiksmas(ref Kryziazodis, Table3);
            KontrolinesSumos(Kryziazodis, ref Table3);



        }/*
        protected void Button2_Click(object sender, EventArgs e)
        {
            ZodziuPaieska.ryskint = true;
            string[] visosEilutes =
File.ReadAllLines(Server.MapPath("App_Data/TextFile1.txt"));

            string input = TextBox1.Text;
            List<List<char>> Kryziazodis = new List<List<char>>();
            List<string> Zodziai = new List<string>();
            List<List<char>> KryziazodisNew = new List<List<char>>();
            DuomenuApdorojimas(ref Kryziazodis, ref Zodziai, visosEilutes);
            ZodziuPaieska.kiekis = -1;
            List<string> NewZodziai = new List<string>();
            NewZodziai.Add(input);
            ZodziuPaieska.Zodziai = NewZodziai;
            Veiksmas(ref KryziazodisNew, Table6);
        }
        */

        //Skaiciuojamos kontrolines sumos - sudedami visi stulepelio skaiciai, tikrinmama ar rezultatas sutampa su skaiciais duotais pavyzdyje
        static void KontrolinesSumos(List<List<char>> Kryziazodis, ref Table table)
        {
            TableRow row = new TableRow();

            for (int j = 0; j < Kryziazodis[0].Count; j++)
            {
                int kiekis = 0;
                for (int i = 0; i < Kryziazodis.Count; i++)
                {
                    string skaicius = Kryziazodis[i][j].ToString();
                    kiekis += int.Parse(skaicius);
                }

                TableCell langelis = new TableCell();
                langelis.Text = kiekis.ToString();
                langelis.Width = 22;
                row.Cells.Add(langelis);
            }
            table.Rows.Add(row);

        }

        static void PradiniaiDuomenys(Table table, List<List<char>> Kryziazodis)
        {
            int kiekis = 0;

            for (int i = 0; i < Kryziazodis.Count; i++)
            {
                char[] masyvas = Kryziazodis[i].ToArray();
                TableRow row = new TableRow();
                foreach (char raide in masyvas)
                {
                    TableCell pavadinimas = new TableCell();
                    pavadinimas.Width = 22;
                    string zodis = raide.ToString();
                    pavadinimas.Text = zodis;

                    row.Cells.Add(pavadinimas);
                }
                TableCell ieskomasZodis = new TableCell();


                string numeris = (kiekis + 1).ToString();
                ieskomasZodis.Text = " / " + numeris + " " + ZodziuPaieska.Zodziai[kiekis];
                row.Cells.Add(ieskomasZodis);
                table.Rows.Add(row);
                kiekis++;
            }
        }

        //Metodas pradedantis naujo zodzio paieska, jei visi zodziai surasti, metodas spausdina rezultatus
        static void Veiksmas(ref List<List<char>> Kryziazodis, Table table)
    {
        ZodziuPaieska.kiekis++;

            if (ZodziuPaieska.kiekis >= ZodziuPaieska.Zodziai.Count)
        {
                if (ZodziuPaieska.ryskint)
                {
                    for (int i = 0; i < Kryziazodis.Count; i++)
                    {
                        char[] masyvas = Kryziazodis[i].ToArray();
                        TableRow row = new TableRow();
                        foreach (char raide in masyvas)
                        {
                            TableCell pavadinimas = new TableCell();
                            pavadinimas.Width = 22;
                        
                                string zodis = raide.ToString();

                            pavadinimas.Text = zodis;
                            if (Char.IsUpper(raide))
                                pavadinimas.Style.Add("font-weight", "bold");

                        }
                        TableCell ieskomasZodis = new TableCell();


                        string numeris = (ZodziuPaieska.kiekis + 1).ToString();
                        row.Cells.Add(ieskomasZodis);
                        table.Rows.Add(row);
                    }
                }
                else
                {
                    ZodziuPaieska.kiekis = 0;
                    for (int i = 0; i < Kryziazodis.Count; i++)
                    {
                        char[] masyvas = Kryziazodis[i].ToArray();
                        TableRow row = new TableRow();
                        foreach (char raide in masyvas)
                        {
                            TableCell pavadinimas = new TableCell();
                            pavadinimas.Width = 22;
                            string zodis = raide.ToString();
                            pavadinimas.Text = zodis;
                            switch (raide)
                            {
                                case '1':
                                    {
                                        pavadinimas.ForeColor = System.Drawing.Color.Aqua;
                                        break;
                                    }

                                case '2':
                                    {
                                        pavadinimas.ForeColor = System.Drawing.Color.Firebrick;
                                        break;
                                    }
                                case '3':
                                    {
                                        pavadinimas.ForeColor = System.Drawing.Color.Chocolate;
                                        break;
                                    }
                                case '4':
                                    {
                                        pavadinimas.ForeColor = System.Drawing.Color.DarkGreen;
                                        break;
                                    }
                                case '5':
                                    {
                                        pavadinimas.ForeColor = System.Drawing.Color.DarkOrange;
                                        break;
                                    }
                            }
                            row.Cells.Add(pavadinimas);
                        }
                        TableCell ieskomasZodis = new TableCell();

                        switch (ZodziuPaieska.kiekis)
                        {
                            case 0:
                                {
                                    ieskomasZodis.ForeColor = System.Drawing.Color.Aqua;
                                    break;
                                }

                            case 1:
                                {
                                    ieskomasZodis.ForeColor = System.Drawing.Color.Firebrick;
                                    break;
                                }
                            case 2:
                                {
                                    ieskomasZodis.ForeColor = System.Drawing.Color.Chocolate;
                                    break;
                                }
                            case 3:
                                {
                                    ieskomasZodis.ForeColor = System.Drawing.Color.DarkGreen;
                                    break;
                                }
                            case 4:
                                {
                                    ieskomasZodis.ForeColor = System.Drawing.Color.DarkOrange;
                                    break;
                                }
                        }
                        string numeris = (ZodziuPaieska.kiekis + 1).ToString();
                        ieskomasZodis.Text = " / " + numeris + " " + ZodziuPaieska.Zodziai[ZodziuPaieska.kiekis];
                        row.Cells.Add(ieskomasZodis);
                        table.Rows.Add(row);
                        ZodziuPaieska.kiekis++;
                    }

                }
            }
            else
        {
            Zodis zodis = new Zodis(ZodziuPaieska.Zodziai[ZodziuPaieska.kiekis]);

            Paieska(table, zodis, ref Kryziazodis, ZodziuPaieska.kiekis + 1, 0, 0, ' ');
        }
    }



        //Is nuskaityto tekstinio failo atrenkama kryziazodzio matrica ir ieskomi zodziai
        static void DuomenuApdorojimas(ref List<List<char>> Kryziazodis, ref List<string> Zodziai, string[] Eilutes)
    {
        string zodis = null;

        List<char> Raides = new List<char>();
        foreach (string line in Eilutes)
        {
            if (Char.IsLetter(line[0]))
            {
                Raides = line.ToList<char>();
                Kryziazodis.Add(Raides);
            }
            else
            {
                zodis = line.Substring(1);
                Zodziai.Add(zodis);
            }
        }
    }

        /// <summary>
        /// Kryziazodyje ieskoma pirma ieskomo zodzio raide
        /// </summary>
        /// <param name="table"></param>
        /// <param name="zodis"></param>Ieskomo zodzio objektas
        /// <param name="Kryziazodis"></param>Kryziazodzio matrica
        /// <param name="skaicius"></param>Skaicius kuri reikia irasyti
        /// <param name="pradziaEilutes"></param>Eilute nuo kurios reikia pradeti paieska
        /// <param name="pradziaStulpelio"></param>Stulpelis nuo kurio reikia pradeti paieska
        ///Metodas gali buti kvieciamas pakartotinai, siu parametru deka metodas prades pakartotina paieska nuo ten, 
        ///kur ja pabaige
        /// 
        /// <param name="raide"></param>raide kurios ieskoma, jeigu sis parametras paduodamas kaip tuscias tarpas, reiskia 
        /// metodas kvieciamas pirma karta ir programa pati pasiims pirma ieskomo zodzio raide, jeigu siuo parametru
        /// perduodamas simbolis, reiskia metodas yra kvieciamas pakartotinai ir paieska prasides nuo ten, kur ji pasibaige.
        public static void Paieska(Table table, Zodis zodis, ref List<List<char>> Kryziazodis, int skaicius, int pradziaEilutes, int pradziaStulpelio, char raide)
        {
            if (raide == ' ')
                raide = zodis.Gauti();
            else
            {
                if (pradziaStulpelio < Kryziazodis[pradziaEilutes].Count - 1)
                    pradziaStulpelio++;
                else
                {
                    pradziaStulpelio = 0;
                    pradziaEilutes++;
                }
            }

            for (int i = pradziaEilutes; i < Kryziazodis.Count; i++)
            {
                for (int j = pradziaStulpelio; j < Kryziazodis[i].Count; j++)
                {
                    if (Kryziazodis[i][j] == raide)
                    {
                        string line = skaicius.ToString();
                        char irasomasSkaicius = Convert.ToChar(line);
                        if (ZodziuPaieska.ryskint)
                            Kryziazodis[i][j] = Char.ToUpper(Kryziazodis[i][j]);
                        Kryziazodis[i][j] = irasomasSkaicius;
                        zodis.PriskirtiRaidesKoordinates(i, j);
                        Pildymas(table, i, j, ref Kryziazodis, zodis, false, skaicius);
                        return;
                    }
                }
                pradziaStulpelio = 0;
            }
        }


        /// <summary>
        /// Suradus ir irasius bent viena ieskomo zodzio raide, aplink ja ieskoma sekancios raides. Klases 
        /// Raide objektas atitinkantis ieskoma raide savyje saugoja visas koordinates kuriose bus surasta raide. Sis metodas 
        /// kartojamas rekursiskai, tol, kol zodis nera surandamas iki galo.
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="eilute"></param>
        /// <param name="stulpelis"></param>
        /// <param name="Kryziazodis"></param>
        /// <param name="zodis"></param>
        /// <param name="nerasta"></param>Parametras nurodantis ar aplink paskutine uzpildyta kryziazodzio pozicija buvo 
        /// rasta reikiama raide. Jeigu parametro reiksme yra TRUE, paskutine uzpildyta kryziazodzio pozicija yra istrinama
        /// ir yra tikrinama ar Klases Raide objektas, atitinkantis ta pacia raide, turi kitas tinkamas koordinates.
        /// <param name="skaicius"></param>
        public static void Pildymas(Table table, int eilute, int stulpelis, ref List<List<char>> Kryziazodis, Zodis zodis, bool nerasta, int skaicius)
    {
           
       //Suradus pilna zodi, pereinama prie kito zodzio paieskos
        if (zodis.kiekis == zodis.zodis.Length - 1)
            Veiksmas(ref Kryziazodis, table);

        else
        {
                if (nerasta)
                {
                    Kryziazodis[eilute][stulpelis] = zodis.Raides[zodis.kiekis].raide;
                    Tuple<int, int> Koordinates = zodis.GautiKoordinates(ref Kryziazodis, zodis, skaicius, table);
                    if (Koordinates == null)
                        return;
                    string line = skaicius.ToString();
                    char irasomasSkaiius = Convert.ToChar(line);
                    if (ZodziuPaieska.ryskint)
                        Kryziazodis[Koordinates.Item1][Koordinates.Item2] = Char.ToUpper(Kryziazodis[Koordinates.Item1][Koordinates.Item2]);
                    else
                    Kryziazodis[Koordinates.Item1][Koordinates.Item2] = irasomasSkaiius;

                    Pildymas(table, Koordinates.Item1, Koordinates.Item2, ref Kryziazodis, zodis, false, skaicius);
                }
                else
                {
                    bool arYra = false;
                    char raide = zodis.Gauti();

                    if (eilute < Kryziazodis.Count - 1 && Kryziazodis[eilute + 1][stulpelis] == raide)
                    {
                        zodis.KoordinaciuIssaugojimas(eilute + 1, stulpelis);
                        arYra = true;
                    }
                    if (eilute != 0 && Kryziazodis[eilute - 1][stulpelis] == raide)
                    {
                        zodis.KoordinaciuIssaugojimas(eilute - 1, stulpelis);
                        arYra = true;
                    }
                    if (stulpelis < Kryziazodis[eilute].Count - 1 && Kryziazodis[eilute][stulpelis + 1] == raide)
                    {
                        zodis.KoordinaciuIssaugojimas(eilute, stulpelis + 1);
                        arYra = true;
                    }
                    if (stulpelis != 0 && Kryziazodis[eilute][stulpelis - 1] == raide)
                    {
                        zodis.KoordinaciuIssaugojimas(eilute, stulpelis - 1);
                        arYra = true;
                    }
                    if (arYra)
                    {
                        Tuple<int, int> Koordinates = zodis.GautiKoordinates(ref Kryziazodis, zodis, skaicius, table);
                        string line = skaicius.ToString();
                        char irasomasSkaicius = Convert.ToChar(line);
                        if (ZodziuPaieska.ryskint)
                            Kryziazodis[Koordinates.Item1][Koordinates.Item2] = Char.ToUpper(Kryziazodis[Koordinates.Item1][Koordinates.Item2]);
                        else
                        Kryziazodis[Koordinates.Item1][Koordinates.Item2] = irasomasSkaicius;

                        zodis.PriskirtiRaidesKoordinates(Koordinates.Item1, Koordinates.Item2);

                        Pildymas(table, Koordinates.Item1, Koordinates.Item2, ref Kryziazodis, zodis, false, skaicius);
                    }
                    else
                    {
                        zodis.ZingsnisAtgal();
                        Pildymas(table, eilute, stulpelis, ref Kryziazodis, zodis, true, skaicius);
                    }
                }
        }
    }




    }
}
    

